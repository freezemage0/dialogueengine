class Application:
	def __init__(self):
		self.__initParams = {
			'event_name': 'onApplicationStart',
			'event_params': {}
		}
		self.evt = EventManager.getInstance()
		self.ent = EntityManager.getInstance()

	def initialize(self):
		entityNames = self.ent.getEntities()
		for name in entityNames:
			entity = self.ent.getEntity(name)
			entity.initialize()
		
		handlers = {
			'onExit': self.exit,
			'onError': self.error
		}
		self.evt.registerHandlers(handlers)

	def run(self):
		params = self.__initParams
		while True:
			evt = Event(params)
			params = evt.send()

	def error(self, params):
		"""
		Error handling here JUST before the exit
		"""
		self.exit(params)

	def exit(self, params):
		msg = params['message']
		exit(msg)
