class EntityManager:
	'''
	Singleton representing pool of entities
	'''
	class __EntityManager:
		_entities = {} 
		def registerEntity(self, entity):
			try:
				entityName = entity.getName()
			except:
				raise Exception('Entity does not implement getName() method')
			entities = self.getAll()
			if entityName in entities:
				raise AttributeError('Entity "{}" already registered'.format(entityName))
			self._entities[entityName] = entity
			
		def getEntities(self):
			entities = self.getAll()
			return list(dict_keys(entities))

		def getAll(self):
			return self._entities
		
		def getEntity(self, entityName):
			entities = self.getAll()
			if not entityName in entities:
				raise AttributeError('Entity "{}" is not registered or does not exist'.format(entityName))
			entity = entities[entityName]
			return entity

	__instance = None
	
	def __init__():
		raise NotImplementedError()
	
	def getInstance():
		if EntityManager.__instance == None:
			EntityManager.__instance = EntityManager.__EntityManager()
		return EntityManager.__instance
	
class Entity:
	def getName():
		return self.__class__.__name__
