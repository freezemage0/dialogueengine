class EventManager:
	'''
	Singleton representing pool of entities
	'''
	class __EventManager:
		_handlers = {} 
		def registerHandler(self, eventName, handler):
			handlers = self.getAll()

			if eventName in handlers:
				raise AttributeError('Handler for Event "{}" is already registered'.format(eventName))
			self._handlers[eventName] = handler
			
		def registerHandlers(self, handlers):
			for h in handlers:
				eventName = h
				handler = handlers[h]
				
				self.registerHandler(eventName, handler) 

		def getAll(self):
			return self._handlers
		
		def getHandler(self, eventName):
			handlers = self.getAll()
			if eventName not in handlers:
				raise AttributeError('No handlers registered for Event "{}"'.format(eventName))
			return handlers[eventName]

		def receive(self, event):
			eventName = event.getName()
			params = event.getParams()
			
			handler = self.getHandler(eventName)
			return handler(params)

	__instance = None
	
	def __init__():
		raise NotImplementedError()
	
	def getInstance():
		if EventManager.__instance == None:
			EventManager.__instance = EventManager.__EventManager()
		return EventManager.__instance
	
class Event:
	def __init__(self, name, params):
		self.__name = name
		self.__params = params

	def getName(self):
		return self.__name

	def getParams(self):
		return self.__params

	def send(self):
		evtmng = EventManager.getInstance()
		return evtmng.receive(self) 
