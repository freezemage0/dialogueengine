# Branches
## master
Main branch

## dev
In development


# Files
## entitymanager.py
EntityManager: Singleton controlling entities pool
Entity: base class for all entities. All entities should be derived from this class
## eventmanager.py
EventManager: Singletron controlling handlers pool
Event: class for creating events and sending them to EventManager receiver
